# GHC/head.hackage CI support
# ===========================
#
# This is the GitLab CI automation that drives GHC's head.hackage testing.
# The goal is to be able to test GHC by building a subset of Hackage.
# Moreover, we want to be able to collect logs of failed builds as well as
# performance metrics from builds that succeed.
#
# To accomplish this we use the ci executable in ./ci. This drives a set of
# cabal v2-build builds and preserves their results.
#
# The compiler to be tested can be taken from a number of sources. The
# build-master, build-9.0, and build-9.2 jobs form the validation pipeline of the
# head.hackage repository. In addition, other GitLab projects (e.g. ghc/ghc>)
# can trigger a multi-project pipeline, specifying a GHC binary distribution
# via either the GHC_TARBALL or UPSTREAM_* variables.

stages:
  - test
  - update-repo
  - deploy

variables:
  # Which nixos/nix Docker image tag to use
  DOCKER_TAG: "2.3"

  # Default GHC bindist
  GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/master/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"

  # Default this to ghc/ghc> to make it more convenient to run from the web
  # interface.
  UPSTREAM_PROJECT_ID: 1
  UPSTREAM_PROJECT_PATH: "ghc/ghc"

  # CPUS is set by the runner, as usual.

  # EXTRA_HC_OPTS are passed to via --ghc-options to GHC during the package
  # builds. This is instantiated with, e.g., -dcore-lint during GHC validation
  # builds.

  # ONLY_PACKAGES can be passed to restrict set of packages that are built.

  # EXTRA_OPTS are passed directly to test-patches.

  # Multi-project pipeline variables:
  #
  # These are set by the "upstream" pipeline for `build-pipeline` pipelines:
  #
  #   UPSTREAM_PROJECT_PATH: The path of the upstream project (e.g. `ghc/ghc`)
  #   UPSTREAM_PIPELINE_ID: The ID of the upstream pipeline
  #
  # Instead of UPSTREAM_PIPELINE_ID you can also pass:

  #   UPSTREAM_COMMIT_SHA: The ref or commit SHA of the GHC build to be tested
  #


# A build triggered from a ghc/ghc> pipeline.
build-pipeline:
  extends: .build
  before_script:
    - |
      if [ -n "$UPSTREAM_COMMIT_SHA" ]; then
        # N.B. We can't use this if the upstream pipeline might be in-progress
        # since the below URL cannot provide an artifact until a pipeline has
        # run to completion on the requested branch. This is in general
        # not the case for GHC pipelines. Consequently, in this case we will
        # usually rather provide UPSTREAM_PIPELINE_ID.
        echo "Pulling binary distribution from commit $UPSTREAM_COMMIT_SHA of project $UPSTREAM_PROJECT_PATH..."
        GHC_TARBALL="https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/artifacts/$UPSTREAM_COMMIT_SHA/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
      elif [ -n "$UPSTREAM_PIPELINE_ID" ]; then
        job_name="validate-x86_64-linux-fedora27"
        echo "Pulling ${job_name} binary distribution from Pipeline $UPSTREAM_PIPELINE_ID..."
        job_id=$(nix run -f ci/default.nix \
          -c find-job $UPSTREAM_PROJECT_ID $UPSTREAM_PIPELINE_ID $job_name)
        echo "Using job $job_id..."
        GHC_TARBALL="https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/$job_id/artifacts/raw/ghc-x86_64-fedora27-linux.tar.xz"
      fi
  variables:
    EXTRA_HC_OPTS: "-dcore-lint -ddump-timings"
  rules:
    - if: '$UPSTREAM_COMMIT_SHA || $UPSTREAM_PIPELINE_ID'
      when: always
    - when: never

# Build against the master branch
build-master:
  extends: .build
  variables:
    GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/master/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
    EXTRA_HC_OPTS: "-dcore-lint"
  rules:
    - if: '$UPSTREAM_COMMIT_SHA || $UPSTREAM_PIPELINE_ID'
      when: never
    - when: always

# Build against the 9.0 branch
build-9.0:
  extends: .build
  variables:
    GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/ghc-9.0/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
    EXTRA_HC_OPTS: "-dcore-lint"
  rules:
    - if: '$UPSTREAM_COMMIT_SHA || $UPSTREAM_PIPELINE_ID'
      when: never
    - when: always

# Build against the 9.2 branch
build-9.2:
  extends: .build
  variables:
    GHC_TARBALL: "https://gitlab.haskell.org/api/v4/projects/1/jobs/artifacts/ghc-9.2/raw/ghc-x86_64-fedora27-linux.tar.xz?job=validate-x86_64-linux-fedora27"
    EXTRA_HC_OPTS: "-dcore-lint"
  rules:
    - if: '$UPSTREAM_COMMIT_SHA || $UPSTREAM_PIPELINE_ID'
      when: never
    - when: always

.build:
  stage: test

  tags:
    - x86_64-linux

  image: "nixos/nix:$DOCKER_TAG"

  cache:
    key: build-HEAD
    paths:
      - store.nar

  before_script:
    - |
      if [ -e store.nar ]; then
        echo "Extracting cached Nix store..."
        nix-store --import -vv < store.nar || echo "invalid cache"
      else
        echo "No cache found"
      fi

  script:
      # Install GHC
    - echo "Bindist tarball is $GHC_TARBALL"
    - nix run -f ./ci -c curl -L "$GHC_TARBALL" > ghc.tar.xz
    - |
      nix build \
      -f ci/ghc-from-artifact.nix \
      --arg ghcTarball ./ghc.tar.xz \
      --out-link ghc
    - export GHC=`pwd`/ghc/bin/ghc
    - rm -Rf $HOME/.cabal/packages/local ci/run
      # Build CI executable
    - |
      nix-build ./ci -j$CPUS --no-build-output
      nix-store --export \
        $(nix-store -qR --include-outputs \
          $(nix-instantiate --quiet ./ci)) \
      > store.nar
      # Test it
    - nix run -f ./ci -c run-ci

  after_script:
    - ls -lh
    - |
      nix run -f ./ci -c \
      tar -cJf results.tar.xz -C ci/run \
      results.json logs compiler-info

  artifacts:
    when: always
    paths:
    - results.tar.xz

# Build and deploy a Hackage repository
update-repo:
  stage: update-repo

  tags:
    - x86_64-linux
    - head.hackage

  image: "nixos/nix:$DOCKER_TAG"

  variables:
    KEYS_TARBALL: https://downloads.haskell.org/ghc/head.hackage-keys.tar.enc
    # KEYS_TARBALL_KEY provided by protected variable

  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'

  script:
    - nix-channel --add https://nixos.org/channels/nixpkgs-unstable nixpkgs
    - nix-channel --update
    - nix build -f ci/default.nix
    - nix run -f ci/default.nix -c build-repo.sh extract-keys
    - nix run -f ci/default.nix -c build-repo.sh build-repo

  dependencies:
    - build-master

  after_script:
    - rm -Rf keys

  artifacts:
    paths:
      - repo

pages:
  stage: deploy
  tags:
    - x86_64-linux
    - head.hackage
  image: "nixos/nix:$DOCKER_TAG"
  script:
    - mv repo public
  dependencies:
    - update-repo
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  artifacts:
    paths:
      - public
