let sources = import ./nix/sources.nix;
in

{ nixpkgs ? (import sources.nixpkgs.outPath {}) }:

with nixpkgs;
let
  haskellPackages = nixpkgs.haskellPackages.override { all-cabal-hashes = sources.all-cabal-hashes.outPath; };

  hackage-repo-tool =
    let src = sources.hackage-security.outPath;
    in haskellPackages.callCabal2nix "hackage-repo-tool" "${src}/hackage-repo-tool" {
        optparse-applicative = haskellPackages.callHackage "optparse-applicative" "0.15.1.0" {};
      };

  overlay-tool =
    let src = sources.overlay-tool;
    in haskellPackages.callCabal2nix "hackage-overlay-repo-tool" src { };

  head-hackage-ci =
    let
      src = nixpkgs.nix-gitignore.gitignoreSource [] ./.;
    in haskellPackages.callCabal2nix "head-hackage-ci" src {};

  buildDepsFragment =
    let
      buildDeps = import ./build-deps.nix { pkgs = nixpkgs; };

      mkCabalFragment = pkgName: deps:
        with pkgs.lib;
        let
          libDirs = concatStringsSep " " (map (dep: getOutput "lib" dep + "/lib") deps);
          includeDirs = concatStringsSep " " (map (dep: getOutput "dev" dep + "/include") deps);
        in ''
        package ${pkgName}
          extra-lib-dirs: ${libDirs}
          extra-include-dirs: ${includeDirs}
        '';
    in
      pkgs.lib.concatStringsSep "\n"
        (pkgs.lib.mapAttrsToList mkCabalFragment buildDeps);

  buildDepsFile = pkgs.writeText "deps.cabal.project" buildDepsFragment;

  build-repo =
    let
      deps = [
        bash curl gnutar findutils patch rsync openssl
        cabal-install ghc gcc binutils-unwrapped pwgen gnused
        hackage-repo-tool overlay-tool python3 jq
        git # cabal-install wants this to fetch source-repository-packages
      ];
    in
      runCommand "repo" {
        nativeBuildInputs = [ makeWrapper ];
        cabalDepsSrc = buildDepsFragment;
      } ''
        mkdir -p $out/bin
        makeWrapper ${head-hackage-ci}/bin/head-hackage-ci $out/bin/head-hackage-ci \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${./build-repo.sh} $out/bin/build-repo.sh \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${../run-ci} $out/bin/run-ci \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin \
            --set USE_NIX 1 \
            --set CI_CONFIG ${./config.sh}

        makeWrapper ${./find-job.sh} $out/bin/find-job \
            --prefix PATH : ${stdenv.lib.makeBinPath deps}:$out/bin

        makeWrapper ${xz}/bin/xz $out/bin/xz
        makeWrapper ${curl}/bin/curl $out/bin/curl
      '';
in
  build-repo
