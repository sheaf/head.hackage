diff --git a/src/Data/Parameterized/Context/Unsafe.hs b/src/Data/Parameterized/Context/Unsafe.hs
index f44f521..ec44346 100644
--- a/src/Data/Parameterized/Context/Unsafe.hs
+++ b/src/Data/Parameterized/Context/Unsafe.hs
@@ -1,5 +1,6 @@
 {-# LANGUAGE CPP #-}
 {-# LANGUAGE DataKinds #-}
+{-# LANGUAGE FlexibleContexts #-}
 {-# LANGUAGE FlexibleInstances #-}
 {-# LANGUAGE InstanceSigs #-}
 {-# LANGUAGE GADTs #-}
diff --git a/src/Data/Parameterized/NatRepr.hs b/src/Data/Parameterized/NatRepr.hs
index 94db5bf..4e95448 100644
--- a/src/Data/Parameterized/NatRepr.hs
+++ b/src/Data/Parameterized/NatRepr.hs
@@ -129,6 +129,8 @@ module Data.Parameterized.NatRepr
   ) where
 
 import Data.Bits ((.&.), bit)
+import Data.Constraint (Dict(..))
+import Data.Constraint.Nat (zeroLe)
 import Data.Data
 import Data.Type.Equality as Equality
 import Data.Void as Void
@@ -346,7 +348,7 @@ withSubMulDistribRight _n _m _p f =
 
 -- | @LeqProof m n@ is a type whose values are only inhabited when @m@
 -- is less than or equal to @n@.
-data LeqProof m n where
+data LeqProof (m :: Nat) (n :: Nat) where
   LeqProof :: (m <= n) => LeqProof m n
 
 -- | (<=) is a decidable relation on nats.
@@ -475,14 +477,14 @@ leqMulMono x y = leqMulCongr (leqProof (Proxy :: Proxy 1) x) (leqRefl y)
 -- | Produce proof that adding a value to the larger element in an LeqProof
 -- is larger
 leqAdd :: forall f m n p . LeqProof m n -> f p -> LeqProof m (n+p)
-leqAdd x _ = leqAdd2 x (LeqProof :: LeqProof 0 p)
+leqAdd x _ = case zeroLe @p of Dict -> leqAdd2 x (LeqProof :: LeqProof 0 p)
 
 leqAddPos :: (1 <= m, 1 <= n) => p m -> q n -> LeqProof 1 (m + n)
 leqAddPos m n = leqAdd (leqProof (Proxy :: Proxy 1) m) n
 
 -- | Produce proof that subtracting a value from the smaller element is smaller.
 leqSub :: forall m n p . LeqProof m n -> LeqProof p m -> LeqProof (m-p) n
-leqSub x _ = leqSub2 x (LeqProof :: LeqProof 0 p)
+leqSub x _ = case zeroLe @p of Dict -> leqSub2 x (LeqProof :: LeqProof 0 p)
 
 addIsLeq :: f n -> g m -> LeqProof n (n + m)
 addIsLeq n m = leqAdd (leqRefl n) m
diff --git a/src/Data/Parameterized/TH/GADT.hs b/src/Data/Parameterized/TH/GADT.hs
index fc6563c..73b4069 100644
--- a/src/Data/Parameterized/TH/GADT.hs
+++ b/src/Data/Parameterized/TH/GADT.hs
@@ -8,6 +8,7 @@
 -- This module declares template Haskell primitives so that it is easier
 -- to work with GADTs that have many constructors.
 ------------------------------------------------------------------------
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE DoAndIfThenElse #-}
 {-# LANGUAGE GADTs #-}
 {-# LANGUAGE MultiParamTypeClasses #-}
@@ -62,7 +63,7 @@ conPat ::
   Q (Pat, [Name]) {- ^ pattern and bound names -}
 conPat con pre = do
   nms <- newNames pre (length (constructorFields con))
-  return (ConP (constructorName con) (VarP <$> nms), nms)
+  return (conPCompat (constructorName con) (VarP <$> nms), nms)
 
 
 -- | Return an expression corresponding to the constructor.
@@ -483,7 +484,7 @@ structuralShowsPrec tpq = do
 showCon :: ExpQ -> Name -> Int -> MatchQ
 showCon p nm n = do
   vars <- newNames "x" n
-  let pat = ConP nm (VarP <$> vars)
+  let pat = conPCompat nm (VarP <$> vars)
   let go s e = [| $(s) . showChar ' ' . showsPrec 11 $(varE e) |]
   let ctor = [| showString $(return (LitE (StringL (nameBase nm)))) |]
   let rhs | null vars = ctor
@@ -532,3 +533,10 @@ matchShowCtor p con = showCon p (constructorName con) (length (constructorFields
 --
 -- The use of 'DataArg' says that the type parameter of the 'NatRepr' must
 -- be the same as the second type parameter of @T@.
+
+conPCompat :: Name -> [Pat] -> Pat
+conPCompat n pats = ConP n
+#if MIN_VERSION_template_haskell(2,18,0)
+                           []
+#endif
+                           pats
diff --git a/src/Data/Parameterized/Vector.hs b/src/Data/Parameterized/Vector.hs
index 9d4b653..c3b93c3 100644
--- a/src/Data/Parameterized/Vector.hs
+++ b/src/Data/Parameterized/Vector.hs
@@ -92,6 +92,8 @@ module Data.Parameterized.Vector
 import qualified Data.Vector as Vector
 import Data.Functor.Compose
 import Data.Coerce
+import Data.Constraint (Dict(..))
+import Data.Constraint.Nat (zeroLe)
 import Data.Vector.Mutable (MVector)
 import qualified Data.Vector.Mutable as MVector
 import Control.Monad.ST
@@ -462,7 +464,7 @@ unfoldrWithIndexM' h gen start =
         snd <$> getCompose3 (natRecBounded (decNat h) (decNat h) base step)
       }
   where base :: Compose3 m ((,) b) (Vector' a) 0
-        base = Compose3 $ (\(hd, b) -> (b, MkVector' (singleton hd))) <$> gen (knownNat @0) start
+        base = case zeroLe @h of Dict -> Compose3 $ (\(hd, b) -> (b, MkVector' (singleton hd))) <$> gen (knownNat @0) start
         step :: forall p. (1 <= h, p <= h - 1)
              => NatRepr p
              -> Compose3 m ((,) b) (Vector' a) p
