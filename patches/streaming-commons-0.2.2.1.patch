diff --git a/Data/Text/Internal/Encoding/Utf16.hs b/Data/Text/Internal/Encoding/Utf16.hs
index e5e3c49..4f929f3 100644
--- a/Data/Text/Internal/Encoding/Utf16.hs
+++ b/Data/Text/Internal/Encoding/Utf16.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE MagicHash, BangPatterns #-}
 
 -- |
@@ -29,8 +30,8 @@ import GHC.Word (Word16(..))
 chr2 :: Word16 -> Word16 -> Char
 chr2 (W16# a#) (W16# b#) = C# (chr# (upper# +# lower# +# 0x10000#))
     where
-      !x# = word2Int# a#
-      !y# = word2Int# b#
+      !x# = word2Int# (word16ToWordCompat# a#)
+      !y# = word2Int# (word16ToWordCompat# b#)
       !upper# = uncheckedIShiftL# (x# -# 0xD800#) 10#
       !lower# = y# -# 0xDC00#
 {-# INLINE chr2 #-}
@@ -43,3 +44,11 @@ validate2       ::  Word16 -> Word16 -> Bool
 validate2 x1 x2 = x1 >= 0xD800 && x1 <= 0xDBFF &&
                   x2 >= 0xDC00 && x2 <= 0xDFFF
 {-# INLINE validate2 #-}
+
+#if MIN_VERSION_base(4,16,0)
+word16ToWordCompat# :: Word16# -> Word#
+word16ToWordCompat# = word16ToWord#
+#else
+word16ToWordCompat# :: Word# -> Word#
+word16ToWordCompat# x = x
+#endif
diff --git a/Data/Text/Internal/Encoding/Utf8.hs b/Data/Text/Internal/Encoding/Utf8.hs
index 5ec469b..952cf30 100644
--- a/Data/Text/Internal/Encoding/Utf8.hs
+++ b/Data/Text/Internal/Encoding/Utf8.hs
@@ -94,8 +94,8 @@ ord4 c =
 chr2 :: Word8 -> Word8 -> Char
 chr2 (W8# x1#) (W8# x2#) = C# (chr# (z1# +# z2#))
     where
-      !y1# = word2Int# x1#
-      !y2# = word2Int# x2#
+      !y1# = word2Int# (word8ToWordCompat# x1#)
+      !y2# = word2Int# (word8ToWordCompat# x2#)
       !z1# = uncheckedIShiftL# (y1# -# 0xC0#) 6#
       !z2# = y2# -# 0x80#
 {-# INLINE chr2 #-}
@@ -103,9 +103,9 @@ chr2 (W8# x1#) (W8# x2#) = C# (chr# (z1# +# z2#))
 chr3 :: Word8 -> Word8 -> Word8 -> Char
 chr3 (W8# x1#) (W8# x2#) (W8# x3#) = C# (chr# (z1# +# z2# +# z3#))
     where
-      !y1# = word2Int# x1#
-      !y2# = word2Int# x2#
-      !y3# = word2Int# x3#
+      !y1# = word2Int# (word8ToWordCompat# x1#)
+      !y2# = word2Int# (word8ToWordCompat# x2#)
+      !y3# = word2Int# (word8ToWordCompat# x3#)
       !z1# = uncheckedIShiftL# (y1# -# 0xE0#) 12#
       !z2# = uncheckedIShiftL# (y2# -# 0x80#) 6#
       !z3# = y3# -# 0x80#
@@ -115,10 +115,10 @@ chr4             :: Word8 -> Word8 -> Word8 -> Word8 -> Char
 chr4 (W8# x1#) (W8# x2#) (W8# x3#) (W8# x4#) =
     C# (chr# (z1# +# z2# +# z3# +# z4#))
     where
-      !y1# = word2Int# x1#
-      !y2# = word2Int# x2#
-      !y3# = word2Int# x3#
-      !y4# = word2Int# x4#
+      !y1# = word2Int# (word8ToWordCompat# x1#)
+      !y2# = word2Int# (word8ToWordCompat# x2#)
+      !y3# = word2Int# (word8ToWordCompat# x3#)
+      !y4# = word2Int# (word8ToWordCompat# x4#)
       !z1# = uncheckedIShiftL# (y1# -# 0xF0#) 18#
       !z2# = uncheckedIShiftL# (y2# -# 0x80#) 12#
       !z3# = uncheckedIShiftL# (y3# -# 0x80#) 6#
@@ -166,3 +166,11 @@ validate4 x1 x2 x3 x4 = validate4_1 || validate4_2 || validate4_3
                   between x2 0x80 0x8F &&
                   between x3 0x80 0xBF &&
                   between x4 0x80 0xBF
+
+#if MIN_VERSION_base(4,16,0)
+word8ToWordCompat# :: Word8# -> Word#
+word8ToWordCompat# = word8ToWord#
+#else
+word8ToWordCompat# :: Word# -> Word#
+word8ToWordCompat# x = x
+#endif
diff --git a/Data/Text/Internal/Unsafe/Char.hs b/Data/Text/Internal/Unsafe/Char.hs
index d208e3f..b02d560 100644
--- a/Data/Text/Internal/Unsafe/Char.hs
+++ b/Data/Text/Internal/Unsafe/Char.hs
@@ -32,24 +32,28 @@ import Control.Exception (assert)
 import Control.Monad.ST (ST)
 import Data.Bits ((.&.))
 import Data.Text.Internal.Unsafe.Shift (shiftR)
-import GHC.Exts (Char(..), Int(..), chr#, ord#, word2Int#)
+import GHC.Exts (Char(..), Int(..), Word#, chr#, ord#, word2Int#)
 import GHC.Word (Word8(..), Word16(..), Word32(..))
 import qualified Data.Text.Array as A
 
+#if MIN_VERSION_base(4,16,0)
+import GHC.Exts (Word8#, Word16#, Word32#, word8ToWord#, word16ToWord#, word32ToWord#)
+#endif
+
 ord :: Char -> Int
 ord (C# c#) = I# (ord# c#)
 {-# INLINE ord #-}
 
 unsafeChr :: Word16 -> Char
-unsafeChr (W16# w#) = C# (chr# (word2Int# w#))
+unsafeChr (W16# w#) = C# (chr# (word2Int# (word16ToWordCompat# w#)))
 {-# INLINE unsafeChr #-}
 
 unsafeChr8 :: Word8 -> Char
-unsafeChr8 (W8# w#) = C# (chr# (word2Int# w#))
+unsafeChr8 (W8# w#) = C# (chr# (word2Int# (word8ToWordCompat# w#)))
 {-# INLINE unsafeChr8 #-}
 
 unsafeChr32 :: Word32 -> Char
-unsafeChr32 (W32# w#) = C# (chr# (word2Int# w#))
+unsafeChr32 (W32# w#) = C# (chr# (word2Int# (word32ToWordCompat# w#)))
 {-# INLINE unsafeChr32 #-}
 
 -- | Write a character into the array at the given offset.  Returns
@@ -93,3 +97,23 @@ unsafeWriteRev marr i c
           hi = fromIntegral $ (m .&. 0x3FF) + 0xDC00
 {-# INLINE unsafeWriteRev #-}
 -}
+
+#if MIN_VERSION_base(4,16,0)
+word8ToWordCompat# :: Word8# -> Word#
+word8ToWordCompat# = word8ToWord#
+
+word16ToWordCompat# :: Word16# -> Word#
+word16ToWordCompat# = word16ToWord#
+
+word32ToWordCompat# :: Word32# -> Word#
+word32ToWordCompat# = word32ToWord#
+#else
+word8ToWordCompat# :: Word# -> Word#
+word8ToWordCompat# x = x
+
+word16ToWordCompat# :: Word# -> Word#
+word16ToWordCompat# x = x
+
+word32ToWordCompat# :: Word# -> Word#
+word32ToWordCompat# x = x
+#endif
diff --git a/Data/Text/Internal/Unsafe/Shift.hs b/Data/Text/Internal/Unsafe/Shift.hs
index b2fef9b..3746b6b 100644
--- a/Data/Text/Internal/Unsafe/Shift.hs
+++ b/Data/Text/Internal/Unsafe/Shift.hs
@@ -1,3 +1,4 @@
+{-# LANGUAGE CPP #-}
 {-# LANGUAGE MagicHash #-}
 
 -- |
@@ -36,17 +37,17 @@ class UnsafeShift a where
 
 instance UnsafeShift Word16 where
     {-# INLINE shiftL #-}
-    shiftL (W16# x#) (I# i#) = W16# (narrow16Word# (x# `uncheckedShiftL#` i#))
+    shiftL (W16# x#) (I# i#) = W16# (narrow16WordCompat# (word16ToWordCompat# x# `uncheckedShiftL#` i#))
 
     {-# INLINE shiftR #-}
-    shiftR (W16# x#) (I# i#) = W16# (x# `uncheckedShiftRL#` i#)
+    shiftR (W16# x#) (I# i#) = W16# (wordToWord16Compat# (word16ToWordCompat# x# `uncheckedShiftRL#` i#))
 
 instance UnsafeShift Word32 where
     {-# INLINE shiftL #-}
-    shiftL (W32# x#) (I# i#) = W32# (narrow32Word# (x# `uncheckedShiftL#` i#))
+    shiftL (W32# x#) (I# i#) = W32# (narrow32WordCompat# (word32ToWordCompat# x# `uncheckedShiftL#` i#))
 
     {-# INLINE shiftR #-}
-    shiftR (W32# x#) (I# i#) = W32# (x# `uncheckedShiftRL#` i#)
+    shiftR (W32# x#) (I# i#) = W32# (wordToWord32Compat# (word32ToWordCompat# x# `uncheckedShiftRL#` i#))
 
 instance UnsafeShift Word64 where
     {-# INLINE shiftL #-}
@@ -70,3 +71,43 @@ instance UnsafeShift Integer where
     {-# INLINE shiftR #-}
     shiftR = Bits.shiftR
 -}
+
+#if MIN_VERSION_base(4,16,0)
+word16ToWordCompat# :: Word16# -> Word#
+word16ToWordCompat# = word16ToWord#
+
+word32ToWordCompat# :: Word32# -> Word#
+word32ToWordCompat# = word32ToWord#
+
+wordToWord16Compat# :: Word# -> Word16#
+wordToWord16Compat# = wordToWord16#
+
+wordToWord32Compat# :: Word# -> Word32#
+wordToWord32Compat# = wordToWord32#
+
+narrow16WordCompat# :: Word# -> Word16#
+narrow16WordCompat# = wordToWord16#
+
+narrow32WordCompat# :: Word# -> Word32#
+narrow32WordCompat# = wordToWord32#
+#else
+-- No-ops
+word16ToWordCompat# :: Word# -> Word#
+word16ToWordCompat# x = x
+
+word32ToWordCompat# :: Word# -> Word#
+word32ToWordCompat# x = x
+
+wordToWord16Compat# :: Word# -> Word#
+wordToWord16Compat# x = x
+
+wordToWord32Compat# :: Word# -> Word#
+wordToWord32Compat# x = x
+
+-- Actual narrowing
+narrow16WordCompat# :: Word# -> Word#
+narrow16WordCompat# = narrow16Word#
+
+narrow32WordCompat# :: Word# -> Word#
+narrow32WordCompat# = narrow32Word#
+#endif
